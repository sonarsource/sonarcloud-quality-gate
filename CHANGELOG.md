# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.0

- minor: SC-17118 Rebrand repositories owned by the Analysis Experience squad

## 0.1.6

- patch: Correctly support SONAR_QUALITY_GATE_TIMEOUT env variable

## 0.1.5

- patch: Throw specific exception if SONAR_TOKEN is wrong

## 0.1.4

- patch: Fail pipeline with an explicit message if the Quality Gate has not been computed

## 0.1.3

- patch: Do not crash when task response does not contain analysisId

## 0.1.2

- patch: Update readme to clarify the relation with the SonarCloud Scan Pipe

## 0.1.1

- patch: Fine-tune documentation for release

## 0.1.0

- minor: Check quality gate

